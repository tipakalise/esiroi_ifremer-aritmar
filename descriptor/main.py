from data_description import description

if __name__ == '__main__':
    description()

#TODO : use arguments as input of the function (ex: namefile)
#TODO : generate a description for each figure
#TODO : apply the function on each device if there is more than one (ex: generate one report for each device)
#TODO : compress a pdf file because some figures have a large size
#TODO : test the functions based on the work done on the Jupyter notebook
#TODO : resize the figure