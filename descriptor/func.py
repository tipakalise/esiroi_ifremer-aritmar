#########################################################
#Libraries
#########################################################
import os
import _io

import pandas as pd
import matplotlib.pyplot as plt
from pyproj import Geod
import numpy as np

from reportlab.platypus import Flowable
from reportlab.lib.utils import ImageReader

from pdfrw import PdfReader, PdfDict
from pdfrw.buildxobj import pagexobj
from pdfrw.toreportlab import makerl
from matplotlib.backends.backend_pdf import PdfPages

from shapely.geometry import MultiLineString
from shapely.ops import polygonize
import geopandas as gpd
from shapely.geometry import Point
from geopandas.tools import sjoin
import contextily as ctx
from rasterio.crs import CRS
from mpl_toolkits.axes_grid1 import make_axes_locatable

from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score, accuracy_score
from scipy.optimize import least_squares, curve_fit, minimize
from scipy import stats

from sklearn.decomposition import PCA

#########################################################
#Functions
#########################################################
# Preprocessing data
def readDataset(file_name, extension = 'csv'):
	BASE_PATH = './data/'
	dataset = None
	switcher = {
		'csv': pd.read_csv(BASE_PATH+file_name + '.csv')
	}
	dataset = switcher.get(extension.lower(), pd.read_csv(BASE_PATH+file_name + '.csv'))
	return dataset
# end

# Calculate distance
def calculateDist(lat1,lon1,lat2,lon2):
	wgs84_geod = Geod(ellps='WGS84')
	az12,az21,dist = wgs84_geod.inv(lon1,lat1,lon2,lat2)
	return az12, dist
# end

# regression functions
# linear regression
def regr(x, y):
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    return intercept,slope

#end

# least sqares method of evaluation
def objective(p, x, y):
    # calculate y
    y_predict = calc_y(x,p)
    # calculate objective
    obj = 0.0
    for i in range(len(y)):
        obj = obj + ((y_predict[i]-y[i])/y[i])**2    
    # return result
    return obj

def calc_y(x, p):
    a,b = p
    y = - 10*a*np.log10(x) + b
    return y
    
def peval(x,p):
    return -10*p[0]*np.log10(x) + p[1]

def residuals(p,y,x):
    A,B = p
    err = x + 10*A*np.log10(y) - B
    #RSSI = -10nlog10(d) + A
    return err

# end

# chose the regression method
def selectRegrMeth(ch, p,x,y):
    cases = {
        0: lambda a,b,c: least_squares(residuals, a, args=(b,c)),
        1: lambda a,b,c: minimize(objective, a, method='SLSQP', args=(b,c)),
        2: lambda a,b,c: least_squares(residuals, a, loss='soft_l1', f_scale=0.1, args=(b,c)),
        3: lambda a,b,c: least_squares(residuals, a, loss='cauchy', f_scale=0.1, args=(b,c)),
    }
    return cases[ch](p,x,y)
# end

# end

# get sector in polar projection based on the angle in degrees
def sectorIdentifier(row,x):
    angleDeg= row[x]
    angleDeg %=360
    if 0 < angleDeg <= 45:
        val="s1"
    elif 45 < angleDeg <= 90 :    
        val="s2"
    elif 90 < angleDeg <= 135 :      
        val="s3"
    elif 135 < angleDeg <= 180 : 
        val="s4"
    elif 180 < angleDeg <= 225 :
        val="s5"
    elif 225 < angleDeg <= 270 :
        val="s6"
    elif 270 < angleDeg <= 315 : 
        val="s7"
    #elif 315 < angle <= 360 :
    else:
        val="s8"
    return val
# end

# get the distance metric
def selectMetricMethod(method):
	idx1, idx2 = 0,1 #default value (linear regression)
	if method=="regr_linear": idx1, idx2=0,1
	elif method=="lsq": idx1, idx2=1,2
	elif method=="minimize": idx1, idx2=3,4
	elif method=="lsq_soft_l1": idx1, idx2=5,6
	elif method=="lsq_cauchy": idx1, idx2=7,8
	return idx1, idx2

def distanceMetricCalculator(row, gtwName, arrValMetric, method):
	# variables
	nbGtw = len(gtwName)
	splitMetricArr = np.array_split(arrValMetric, nbGtw)
	val, idxLoop, idxLoopMetric1Arr,idxLoopMetric2Arr =0,0,0,1
	coeff=[]

	"""for each graph, we can define the method used to calculate the metric
		by default, the metric used calculates the values using the linear regression method
		if only the name of a method is specified, then this method will be applied to calculate 
		the values for all graph
	"""
	if len(method) != nbGtw:
		if len(method)==0: idx1, idx2=selectMetricMethod(method="regr_linear")
		else : idx1, idx2=selectMetricMethod(method)
		for array in splitMetricArr:
			coeff.append(array[idx1])
			coeff.append(array[idx2])

	for name in gtwName:
		if row['gatewayname'] == name:
			if len(method)==nbGtw:
				idx1, idx2=selectMetricMethod(method=method[idxLoop])
				val = np.power(10,((splitMetricArr[idxLoop][idx1]-row['rssi'])/(10*splitMetricArr[idxLoop][idx2]))) / row['distance']
				idxLoop+=1
			else:
				val = np.power(10,((coeff[idxLoopMetric1Arr]-row['rssi'])/(10*coeff[idxLoopMetric2Arr]))) / row['distance']
				idxLoopMetric1Arr=idxLoopMetric2Arr+1
				idxLoopMetric2Arr+=1
	return val
# end

# plot

#Plot the data where we have displayed multiple data on the y-axis into an x-axis
def plotMulti(data, ax, x, ylabel, cols=None, title=None, xlabel=None,spacing=.1, **kwargs):
	from pandas.plotting._matplotlib.style import get_standard_colors
	
	# Get default color style from pandas - can be changed to any other color list
	if cols is None: cols = data.columns
	if len(cols) == 0: return
	colors = get_standard_colors(num_colors=len(cols))
	# First axis
	data.plot(ax=ax, x=x, y=cols[0], color=colors[0], **kwargs)
	if ylabel is None : ax.set_ylabel(ylabel=cols[0])
	else : ax.set_ylabel(ylabel=ylabel[0], fontsize=8)
	if title is not None : ax.set_title(title)
	if xlabel is not None : ax.set_xlabel(xlabel=xlabel, fontsize=8)
	lines, labels = ax.get_legend_handles_labels()
	
	for n in range(1, len(cols)):
        # Multiple y-axes
		ax_new = ax.twinx()
		ax_new.spines['right'].set_position(('axes', 1 + spacing * (n - 1)))
		data.plot(ax=ax_new, x=x, y=cols[n], color=colors[n % len(colors)], **kwargs)
		if ylabel==None : ax_new.set_ylabel(ylabel=cols[n])
		else : ax_new.set_ylabel(ylabel=ylabel[n])

		# Proper legend position
		line, label = ax_new.get_legend_handles_labels()
		lines += line
		labels += label
	
	#ax.legend(lines, labels, loc=0, fontsize=8)
	return ax

#Plot the data where we have displayed multiple data on the y-axis into an x-axis
def groupedPlotMulti(data, groupby, x='date', cols=None, titlefig=None, ylabel=None, nrows=1,ncols=2, spacing=.1,**kwargs):
	grouped = data.groupby(groupby)
	
	#figure parameters
	fig, axes = plt.subplots(nrows=nrows, ncols=ncols)
	if titlefig is not None : fig.suptitle(str(titlefig), fontsize=12)
	# end

	# plot
	for (key, ax) in zip(grouped.groups.keys(), axes.flatten()):
		tmp = pd.DataFrame(grouped.get_group(key))
		title='gateway=%s'%str(key)
		plotMulti(tmp, ax=ax, x=x, cols=cols, title=title, ylabel=ylabel, xlabel='date', spacing=spacing, **kwargs)
		
	plt.tight_layout(pad=0.5)
	plt.subplots_adjust(top=0.89)
	# end

	# save the figure
	with PdfImageCache() as pdfcache:
		img = pdfcache.savefig(fig, width=480, height=430)
    
	plt.close()

	# end

	return img

#Plot the data with a polar projection
def groupedPolarPlot(data, groupby,x,y,z, titlefig=None, nrows=1,ncols=2, spacing=.1,**kwargs):
	grouped = data.groupby(groupby)
	#figure parameters
	fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(15,21), subplot_kw={'projection': 'polar'}, sharey=True)
	if titlefig is not None : fig.suptitle(str(titlefig), fontsize=12)
	# end

	# plot
	for (key, ax) in zip(grouped.groups.keys(), axes.flatten()):
		tmp = pd.DataFrame(grouped.get_group(key))
		a0 = ax.scatter(tmp[x],tmp[y], c=tmp[z], edgecolors= "white",**kwargs)
		ax.set_title('gateway=%s'%key)
		ax.set_theta_zero_location("N")
		ax.set_theta_direction(-1)
		fig.colorbar(a0, ax=ax)

	plt.tight_layout(pad=0.5)
	plt.subplots_adjust(top=0.89)
	# end

	# save the figure
	with PdfImageCache() as pdfcache:
		img = pdfcache.savefig(fig, width=480, height=430)
    
	plt.close()

	# end

	return img


def simplePlot(data, x, y, xlabel=None, ylabel=None, xminlim=None, xmaxlim=None, plottype='plotDF', titlefig=None, nrows=1, ncols=1, spacing=.1, scalefactor=None, yscale='linear', yerr=None,**kwargs):
	# figure parameters
	fig, axes = plt.subplots(nrows=nrows, ncols=ncols)
	if titlefig is not None : fig.suptitle(str(titlefig), fontsize=13)
	if xlabel is not None : plt.xlabel(xlabel, fontsize=12)
	if ylabel is not None : plt.ylabel(ylabel, fontsize=12)
	# end

	# plot
	if data is not None:
		if plottype=='plotDF': data.plot(x=x,y=y, ax=axes, **kwargs)
		elif plottype=='stepDF' and xmaxlim is None : plt.step(data[x],data[y], **kwargs)
		elif plottype=='boxplotDF': data.boxplot(column=y, by=x, ax=axes, **kwargs)
		elif plottype=='barErrplotDF' and yerr is not None: 
			axes.bar(data[x], data[y],yerr=data[yerr],align='center', alpha=0.5,ecolor='black',capsize=10, **kwargs)
		# zoom
		elif xmaxlim is not None:
			if xminlim is not None: mask = (data[x] >=xminlim) & (data[x] <= xmaxlim )
			else : mask = (data[x] >=min(data[x])) & (data[x] <= xmaxlim )
			tempData = data[mask]
			if plottype=='stepDF':  plt.step(tempData[x],tempData[y],**kwargs)
			else : plt.plot(tempData[x],tempData[y], **kwargs)
		# end

	elif data is None: 
		if plottype=='plot' : axes.plot(x,y, **kwargs)
		# zoom
		elif xmaxlim is not None :
			if xminlim is not None: mask = (x >=xminlim) & (x <= xmaxlim )
			else : mask = (x >=min(x)) & (x <= xmaxlim )
			
			if plottype=='stepDF':  plt.step(x[mask], y[mask], **kwargs)
			else : plt.plot(x[mask], y[mask], **kwargs)
		# end
	# end

	# scale	
	if scalefactor is not None:
		xmin, xmax = plt.xlim()
		plt.xlim(xmin * scalefactor, xmax * scalefactor)

	if yscale != "linear" :
		plt.yscale(yscale)
	# end

	# save the figure
	with PdfImageCache() as pdfcache:
		img = pdfcache.savefig(fig, width=280, height=330)
    
	plt.close()
	# end
	return img


def plotMap(data, dataGtw, titlefig=None, grouped=False, axGrouped=None, spacing=.1,**kwargs):
	if grouped==False and axGrouped==None: fig, ax = plt.subplots(figsize=(10,16)) 
	else : ax=axGrouped
	hb = ax.scatter(x=data['objlng'],y=data['objlat'], alpha=.2,zorder=2,c=data['rssi'], cmap='gist_rainbow_r',**kwargs)
	ax.scatter(dataGtw['gatewaylng'],dataGtw['gatewaylat'],zorder=1,color='red', s=50, label="gateway")

	if titlefig is not None : ax.set_title(titlefig)
	ax.set(xlabel="Longitude", ylabel="Latitude")
	
	ctx.add_basemap(ax, crs=data.crs, reset_extent=True,source=ctx.providers.OpenStreetMap.Mapnik)
	divider = make_axes_locatable(ax)
	cax = divider.append_axes("right", size="5%", pad=0.05)
	plt.colorbar(hb,cax=cax)

	for i in range(dataGtw.shape[0]):
		plt.annotate(dataGtw['gatewayname'].tolist()[i], (dataGtw['gatewaylng'].tolist()[i], dataGtw['gatewaylat'].tolist()[i]))

	ax.legend()
	plt.tight_layout(pad=0.5)
	plt.subplots_adjust(top=0.89)

	if grouped==False :
		with PdfImageCache() as pdfcache:
			img = pdfcache.savefig(fig, width=280, height=330)
			
		plt.close()

		return img
	else: return ax

def groupedPlotMap(data,dataGtw,groupby,titlefig=None, nrows=1,ncols=2, spacing=.1,**kwargs):
	grouped = data.groupby(groupby)
	fig, axes = plt.subplots(nrows=nrows, ncols=ncols)
	if titlefig is not None : fig.suptitle(str(titlefig), fontsize=12)
	for (key, ax) in zip(grouped.groups.keys(), axes.flatten()):
		tmp = gpd.GeoDataFrame(grouped.get_group(key))
		title='gateway=%s'%str(key)
		plotMap(tmp, dataGtw, titlefig=title, axGrouped=ax,grouped=True,**kwargs)
		
	plt.tight_layout(pad=0.5)
	plt.subplots_adjust(top=0.89)

	with PdfImageCache() as pdfcache:
		img = pdfcache.savefig(fig, width=480, height=430)
    
	plt.close()

	return img

def groupedRegrMethodPlot(data, groupby,xdata,ydata,titlefig=None, nrows=1,ncols=2,spacing=.1,xlabel=None, ylabel=None,**kwargs):
	grouped = data.groupby(groupby)
	fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(110, 40), sharey=False)

	arrCoeff = []

	for (key, ax) in zip(grouped.groups.keys(), axes.flatten()):
		tmp = pd.DataFrame(grouped.get_group(key))
		tmp.plot.scatter(ax=ax,x=xdata,y=ydata,label='measured data')
		y = np.array(tmp[xdata])
		X = np.array(tmp[ydata])
		
		#Linear regression
		intercept, slope=regr(X,y)
		ax.plot(X, intercept + slope*X, linewidth=3, color='black',
				label='linear fit: n=%5.4f, txPower=%5.4f' % (slope, intercept),**kwargs)
		arrCoeff.append(intercept)
		arrCoeff.append(slope)

		#Non-linear regression - least square
		p0 = np.array([0.0, 0.0])
		p = selectRegrMeth(0,p0,X,y)
		ax.plot(X, calc_y(X, p.x),'g--.',
				label='non-linear least square fit : n=%5.4f, txPower=%5.4f' % (p.x[0],p.x[1]),**kwargs)
		arrCoeff.append(p.x[0])
		arrCoeff.append(p.x[1])

		#Non-linear regression - minimize slsqp
		p0.fill(0.0)
		p = selectRegrMeth(1,p0,X,y)
		ax.plot(X, calc_y(X,p.x),'--', color='orange',
				label='non-linear minimize slsqp fit : n=%5.4f, txPower=%5.4f' % (p.x[0],p.x[1]),**kwargs)
		arrCoeff.append(p.x[0])
		arrCoeff.append(p.x[1])

		#Non-linear regression - least square soft_l1
		p0.fill(0.0)
		p = selectRegrMeth(2,p0,X,y)
		ax.plot(X, peval(X, p.x),'m--',
				label='non-linear least square soft_l1 fit : n=%5.4f, txPower=%5.4f' % (p.x[0],p.x[1]),**kwargs)
		arrCoeff.append(p.x[0])
		arrCoeff.append(p.x[1])

		#Non-linear regression - least square cauchy
		p0.fill(0.0)
		p = selectRegrMeth(3,p0,X,y)
		ax.plot(X, peval(X, p.x),'c--',
				label='non-linear cauchy fit: n=%5.4f, txPower=%5.4f' % (p.x[0],p.x[1]),**kwargs)
		arrCoeff.append(p.x[0])
		arrCoeff.append(p.x[1])
		
		ax.set_title('gateway=%s'%key)
		if xlabel is not None : ax.set_xlabel(xlabel=xlabel, fontsize=8)
		if ylabel is not None : ax.set_xlabel(xlabel=xlabel, fontsize=8)
		ax.legend(loc='upper right', borderpad=2)

	with PdfImageCache() as pdfcache:
		img = pdfcache.savefig(fig, width=480, height=430)
    
	plt.close()

	return arrCoeff,img

def groupedBoxPlot(data, x,y, groupby,xlabelValue=None, xlabel=None,titlefig=None, nrows=1, ncols=2, spacing=.1,**kwargs):
	grouped = data.groupby(groupby)
	labels=[]
	fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(21,21), sharey=True)
	if titlefig is not None : fig.suptitle(str(titlefig), fontsize=13)
	if xlabelValue is not None :labels=xlabelValue

	for (key, ax) in zip(grouped.groups.keys(), axes.flatten()):
		tmp=pd.DataFrame(grouped.get_group(key))
		tmp.boxplot(ax=ax,column=y, by=x)
		ax.set_xticklabels(labels, rotation = 45)
		ax.set_title('gateway=%s'%key)
		if xlabel is not None : ax.set_xlabel(xlabel)

	plt.suptitle('')

	with PdfImageCache() as pdfcache:
		img = pdfcache.savefig(fig, width=480, height=430)
    
	plt.close()

	return img

def pcaPlot(data,x):
	pca = PCA(n_components=2)
	principalComponents = pca.fit_transform(x)
	principalDf = pd.DataFrame(data = principalComponents, columns = ['pca1', 'pca2'])
	finalDf = pd.concat([principalDf, data[['target']]],axis=1)

	fig, ax = plt.subplots(figsize=(18,21))
	ax.set_xlabel('Principal component 1')
	ax.set_ylabel('Principal component 2')
	ax.set_title('2 components PCA')

	targets = data['target'].unique()
	targets = sorted(targets)

	for target in targets:
		indicesToKeep = finalDf['target']==target
		ax.scatter(finalDf.loc[indicesToKeep, 'pca1'],
				finalDf.loc[indicesToKeep, 'pca2'])

"""
The following functions take only 4 gateways as parameters
"""
def groupedGdfWithBackgrdGridGdfPlot(data1,z1, groupby,data2=None,titlefig=None,groupedTitleText=None, nrows=1, ncols=2, spacing=.1,annotate=False,conditionAnnot=None,colorCmap='viridis',**kwargs):
	grouped = data1.groupby(groupby)
	fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(18,21),sharey=False)
	if titlefig is not None : fig.suptitle(str(titlefig), fontsize=13)

	for (key, ax) in zip(grouped.groups.keys(), axes.flatten()):
		tmp = gpd.GeoDataFrame(grouped.get_group(key))
		tmp.plot(z1, cmap=colorCmap, ax=ax, zorder=2, edgecolors= "white",legend=True,**kwargs) 
		if data2 is not None: data2.plot(ax=ax,facecolor='gray',alpha=.1,edgecolor='black',lw=2,zorder=1)
		ax.set(xlabel="Longitude", ylabel="Latitude")
		if groupedTitleText is not None: ax.set_title('%s=%s'%(str(groupedTitleText),str(key)))

		if annotate==True and conditionAnnot is not None:
			res = data1.loc[data1[conditionAnnot]==key]
			for idx, row in res.iterrows():
				ax.annotate(text=r"%.3f(%d)"%(row[z1],row['idcell']), xy=row['coords'],xytext=row['coords'], xycoords='data',textcoords='data')

	with PdfImageCache() as pdfcache:
		img = pdfcache.savefig(fig, width=480, height=430)
    
	plt.close()

	return img

def gdfWithBackgrdGridGdfPlot(data1,z1, groupby,data2=None,titlefig=None,annotate=False,**kwargs):
	fig, ax = plt.subplots(figsize=(18,21))
	if data2 is not None: data2.plot(ax=ax,facecolor='gray',alpha=.1,edgecolor='black',lw=2)
	data1.plot(ax=ax,**kwargs)
	ax.set(xlabel="Longitude", ylabel="Latitude")
	if titlefig is not None: ax.set_title(titlefig)

	if annotate==True:	
		for idx, row in data1.iterrows():
			ax.annotate(text=str(row['count'])+" ("+str(row['idcell'])+")", xy=row['coords'], horizontalalignment='center')
	
	with PdfImageCache() as pdfcache:
		img = pdfcache.savefig(fig, width=480, height=430)
    
	plt.close()

	return img

# end

# create grid
def createGrid(data,x,y,nbCell):
	M = np.linspace(data[x].min(), data[x].max(), nbCell)
	N = np.linspace(data[y].min(), data[y].max(), nbCell)

	hlines = [((x1, yi), (x2, yi)) for x1, x2 in zip(M[:-1], M[1:]) for yi in N]
	vlines = [((xi, y1), (xi, y2)) for y1, y2 in zip(N[:-1], N[1:]) for xi in M]

	polys = list(polygonize(MultiLineString(hlines + vlines)))
	idx = [i for i in range(len(polys))]
	grid = gpd.GeoDataFrame({"idcell":idx,"geometry":polys})
	return grid
# end

# signature
def checkList(l):
    if not l:
        return ['None']
    else:
        return l


def createTuplesByGtw(row, column_name, gtwName, method="None"):
	tuples = list()
	list_id = row['idcell'].unique()
	list_id = sorted(list_id)
	if gtwName != 4: return
	for idx in list_id :
		arr1 = row[(row['gatewayname']==gtwName[0]) & (row['idcell']==idx)][column_name].tolist()
		arr2 = row[(row['gatewayname']==gtwName[1]) & (row['idcell']==idx)][column_name].tolist()
		arr3 = row[(row['gatewayname']==gtwName[2]) & (row['idcell']==idx)][column_name].tolist()
		arr4 = row[(row['gatewayname']==gtwName[3]) & (row['idcell']==idx)][column_name].tolist()

		if method=="None":
			for x,y,z,e in [(x,y,z,e) for x in checkList(arr1) for y in checkList(arr2) for z in checkList(arr3) for e in checkList(arr4)]:
				value = (x, y,z,e)
				tuples.append({ 'idcell':idx, 'val':value })
				
		elif method=="mean":
			value = ( np.nanmean(arr1), np.nanmean(arr2) , np.nanmean(arr3), np.nanmean(arr4) )
			tuples.append({ 'idcell':idx, 'val':value })
				
		elif method=="percentiles":
			value = ( np.nanpercentile(arr1,25), np.nanpercentile(arr1,75), 
					np.nanpercentile(arr2,25), np.nanpercentile(arr2,75), 
					np.nanpercentile(arr3,25), np.nanpercentile(arr3,75),
					np.nanpercentile(arr4,25), np.nanpercentile(arr4,75))
			
			tuples.append({ 'idcell':idx, 'val1':value[0], 'val2':value[1], 'val3':value[2], 
							'val4':value[3], 'val5':value[4], 'val6':value[5], 'val7':value[6], 
							'val8':value[7] })
	return pd.DataFrame(tuples)


def nbRssiInCellHist(data,gtwName):
	pointInPolysGroupedRSSI =  data.groupby('groupRssi')
	pointInPolysGroupedGateway =  data.groupby('gatewayname')
	pointInPolysGroupedId =  data.groupby('idcell')

	histDict = {}

	for currssi in pointInPolysGroupedRSSI.groups.keys():
		if not currssi in histDict:
			histDict[currssi] = {}
		# initialise count to 0
		for gw in pointInPolysGroupedGateway.groups.keys():
			for curid in pointInPolysGroupedId.groups.keys():
				histDict[currssi][(curid,gw)] = 0
		for index, row in pointInPolysGroupedRSSI.get_group(currssi).iterrows():
			histDict[currssi][(row['idcell'],row['gatewayname'])]+=1
	dataFrameReadyDict = {'idcell':[],'groupRssi':[],'gatewayname':[],'count':[]}
	for currssi in pointInPolysGroupedRSSI.groups.keys():
		for curgw in pointInPolysGroupedGateway.groups.keys():
			for curid in pointInPolysGroupedId.groups.keys():
				dataFrameReadyDict['idcell'].append(curid)
				dataFrameReadyDict['groupRssi'].append(currssi)
				dataFrameReadyDict['gatewayname'].append(curgw)
				dataFrameReadyDict['count'].append(histDict[currssi][(curid,curgw)])

	# convert to dataframe
	dataFrameReadyDictGW = {'idcell':[],'groupRssi':[],gtwName[0]:[],gtwName[1]:[],gtwName[2]:[],gtwName[3]:[]}
	for currssi in pointInPolysGroupedRSSI.groups.keys():
			for curid in pointInPolysGroupedId.groups.keys():
				dataFrameReadyDictGW['idcell'].append(curid)
				dataFrameReadyDictGW['groupRssi'].append(currssi)
				dataFrameReadyDictGW[gtwName[0]].append(histDict[currssi][(curid,gtwName[0])])
				dataFrameReadyDictGW[gtwName[1]].append(histDict[currssi][(curid,gtwName[1])])
				dataFrameReadyDictGW[gtwName[2]].append(histDict[currssi][(curid,gtwName[2])])
				dataFrameReadyDictGW[gtwName[3]].append(histDict[currssi][(curid,gtwName[3])])

	rssicountDF = pd.DataFrame(dataFrameReadyDict)
	rssicountDFGW = pd.DataFrame(dataFrameReadyDictGW)
	rssicountDFGroupedRSSI =  rssicountDF.groupby('groupRssi')
	rssicountDFGroupedGateway =  rssicountDF.groupby('gatewayname')
	rssicountDFGroupedId =  rssicountDF.groupby('idcell')
	return rssicountDF,rssicountDFGW,rssicountDFGroupedRSSI,rssicountDFGroupedGateway,rssicountDFGroupedId
# end


# report class
class PdfImageCache(object):
    """
    Saves matplotlib figures to a temporary multi-page PDF file using the 'savefig'
    method. When closed the images are extracted and saved to the attribute 'cache'.
    The temporary PDF file is then deleted. The 'savefig' returns a PdfImage object
    with a pointer to the 'cache' list and an index for the figure. Use of this
    cache reduces duplicated resources in the reportlab generated PDF file.

    Use is similar to matplotlib's PdfPages object. When not used as a context
    manager, the 'close()' method must be explictly called before the reportlab
    document is built.
    """
    def __init__(self):
        self.pdftempfile = '_temporary_pdf_image_cache_.pdf'
        self.pdf = PdfPages(self.pdftempfile)
        self.cache = []
        self.count = 0

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

    def close(self, *args):
        self.pdf.close()
        pages = PdfReader(self.pdftempfile).pages
        pages = [pagexobj(x) for x in pages]
        self.cache.extend(pages)
        os.remove(self.pdftempfile)

    def savefig(self, fig, width=200, height=200):
        self.pdf.savefig(fig)
        index = self.count
        self.count += 1
        return PdfImage(width=width, height=height, cache=self.cache, cacheindex=index)

class PdfImage(Flowable):
	"""
    Generates a reportlab image flowable for matplotlib figures. It is initialized
    with either a matplotlib figure or a pointer to a list of pagexobj objects and
    an index for the pagexobj to be used.
    """
	def __init__(self, fig=None, width=200, height=200, cache=None, cacheindex=0):
		self.img_width = width
		self.img_height = height
		if fig is None and cache is None:
			raise ValueError("Either 'fig' or 'cache' must be provided")
		if fig is not None:
			imgdata = BytesIO()
			fig.savefig(imgdata, format='eps')
			imgdata.seek(0)
			#page, = PdfReader(imgdata).pages
			#image = pagexobj(page)
			image = ImageReader(imgdata)
			self.img_data = image
		else:
			self.img_data = None
		self.cache = cache
		self.cacheindex = cacheindex
	
	def wrap(self, width, height):
		return self.img_width, self.img_height
	
	def drawOn(self, canv, x, y, _sW=0):
		if _sW > 0 and hasattr(self, 'hAlign'):
			a = self.hAlign
			if a in ('CENTER', 'CENTRE', TA_CENTER):
				x += 0.5*_sW
			elif a in ('RIGHT', TA_RIGHT):
				x += _sW
			elif a not in ('LEFT', TA_LEFT):
				raise ValueError("Bad hAlign value " + str(a))
		canv.saveState()
		if self.img_data is not None:
			img = self.img_data
		else:
			img = self.cache[self.cacheindex]
		if isinstance(img, PdfDict):
			xscale = self.img_width / img.BBox[2]
			yscale = self.img_height / img.BBox[3]
			canv.translate(x, y)
			canv.scale(xscale, yscale)
			canv.doForm(makerl(canv, img))
		else:
			canv.drawImage(img, x, y, self.img_width, self.img_height)
		canv.restoreState()

# end

#TODO : generate automatically the number of rows and columns