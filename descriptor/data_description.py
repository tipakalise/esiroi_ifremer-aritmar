#########################################################
#Libraries
#########################################################
#Handle the data
import pandas as pd
from datetime import datetime, timedelta
import numpy as np

#Plot functions
from func import readDataset, calculateDist, simplePlot, groupedPlotMulti, plotMap
from func import groupedPlotMap, groupedPolarPlot, sectorIdentifier, distanceMetricCalculator
from func import createGrid,groupedGdfWithBackgrdGridGdfPlot

#Image
from io import BytesIO

from shapely.geometry import MultiLineString
from shapely.ops import polygonize
import geopandas as gpd
from shapely.geometry import Point
from geopandas.tools import sjoin
import contextily as ctx
from rasterio.crs import CRS
from mpl_toolkits.axes_grid1 import make_axes_locatable

#Report
from reportlab.lib.units import inch, cm
from reportlab.lib.utils import ImageReader
from reportlab.lib.pagesizes import A4, landscape, portrait
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, NextPageTemplate
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.pdfgen.canvas import Canvas
#########################################################
#Main
#########################################################
def description():
    print("test")
    # Report variables
    doc = SimpleDocTemplate('report1.pdf', pagesize=portrait(A4))
    story = []

    # Data reading
    dfDevice = readDataset("donnees_GNSS_Europa")
    dfGtw = readDataset("Suivi_info_gateway_tortue")
    
    #Rename columns
    dfDevice = dfDevice.rename(columns={"lat": "objlat", "lng": "objlng"})
    dfGtw = dfGtw.rename(columns={"Pseudo": "gatewayname", "short ID": "gatewayid", 
    "Longitude":"gatewaylng", "Latitude":"gatewaylat"})
    # end

    ############################################################
    # Filtering
    ############################################################

    #Filter only on the 8 first columns
    dfDevice = dfDevice[dfDevice.columns[0:8]]

    #Get lon/lat coordinates of the gateway
    featuresDfGtw = ['gatewayid','gatewayname','gatewaylng','gatewaylat']
    dfGtw = dfGtw.loc[:,featuresDfGtw]

    dfDevice = pd.merge(dfDevice,dfGtw,on='gatewayid', how='left')

    dfDevice['az12Deg'],dfDevice['distance'] = calculateDist(dfDevice['gatewaylat'].tolist(),dfDevice['gatewaylng'].tolist(),dfDevice['objlat'].tolist(),dfDevice['objlng'].tolist())

    dfDevice['timestamp'] = dfDevice['timestamp'].apply(lambda x: datetime.utcfromtimestamp(x).strftime('%Y-%m-%d %H:%M:%S'))
    dfDevice['timestamp'] = pd.to_datetime(dfDevice['timestamp'])
    
    dfDevice['rssi'] = dfDevice['rssi'].mask(dfDevice['rssi']>0)

    #EHPE
    perc95 = np.percentile(dfDevice['ehpe'], 95)

    ehpeGroupedDf = dfDevice.groupby('ehpe')['gatewayname']\
        .agg('count').pipe(pd.DataFrame)\
            .rename(columns = {'gatewayname': 'count'}).reset_index()

    im= simplePlot(data=ehpeGroupedDf,x='ehpe',y='count',xlabel="EHPE (m)",ylabel="Number of samples",plottype='stepDF')
    story.append(im)

    im= simplePlot(data=ehpeGroupedDf,x='ehpe',y='count',xlabel="EHPE (m)",ylabel="Number of samples",plottype='stepDF',xmaxlim=perc95)
    story.append(im)

    indexNames = dfDevice[dfDevice['ehpe']>perc95].index
    dfDevice.drop(indexNames, inplace = True)
    # end

    im= groupedPlotMulti(dfDevice, 'gatewayname', x='timestamp', cols=['rssi','distance','snr'], nrows=2, figsize=(25,10))
    story.append(im)

    gdf = gpd.GeoDataFrame(dfDevice, crs = "EPSG:4326", geometry=gpd.points_from_xy(dfDevice['objlng'], dfDevice['objlat']))
    im = plotMap(gdf, dfGtw)
    story.append(im)

    im = groupedPlotMap(gdf, dfGtw, 'gatewayname', nrows=2,ncols=2)
    story.append(im)

    # PDF / CDF
    statsPdfCdfDf = dfDevice.groupby(['distance', 'gatewayname'])['distance'].agg('count').pipe(pd.DataFrame).rename(columns = {'distance': 'frequency'})

    # PDF
    statsPdfCdfDf['pdf'] = statsPdfCdfDf['frequency'] / sum(statsPdfCdfDf['frequency'])
    # CDF
    statsPdfCdfDf['cdf'] = statsPdfCdfDf['pdf'].cumsum()
    statsPdfCdfDf = statsPdfCdfDf.reset_index()

    im= groupedPlotMulti(statsPdfCdfDf, 'gatewayname', x='distance', cols=['cdf','pdf'], ncols=2, nrows=2, ylabel=['Cumulatie probability','Density'], figsize=(110, 40))
    story.append(im)

    count, bins_count = np.histogram(dfDevice['distance'], bins=330)
    pdf = count / sum(count)

    im= simplePlot(data=None,x=bins_count[1:],y=pdf,xlabel="distance (m)",ylabel="Density",plottype='plot', color='orange') 
    story.append(im)

    im= simplePlot(data=None,x=bins_count[1:],y=pdf,xlabel="distance (m)",ylabel="Density",plottype='plot', color='orange',scalefactor=1/3)
    story.append(im)

    # end

    # Number of sended messages by gateway
    statsCountMsgDf = dfDevice.groupby('gatewayname')['gatewayname'].agg('count').pipe(pd.DataFrame).rename(columns = {'gatewayname': 'countmsg'}).reset_index()
    im= simplePlot(data=statsCountMsgDf,x='gatewayname',y='countmsg',plottype='plotDF',kind='bar')
    story.append(im)
    # end

    # inter-reception time
    statsFreqInterRecepMsgDf = dfDevice.filter(['timestamp','gatewayname'],axis=1)
    statsFreqInterRecepMsgDf['deltaTs']=statsFreqInterRecepMsgDf.iloc[:,0].diff()/np.timedelta64(1,'m')

    statsFreqInterRecepMsgGroupedByGtwDf = statsFreqInterRecepMsgDf.drop('timestamp',axis=1)\
        .groupby('gatewayname')['deltaTs'].agg(['mean','std']).pipe(pd.DataFrame).reset_index()
    
    im= simplePlot(data=statsFreqInterRecepMsgGroupedByGtwDf,x='gatewayname',y='mean',plottype='plotDF',kind='bar')
    story.append(im)
    
    im= simplePlot(data=statsFreqInterRecepMsgDf,x='gatewayname',y='deltaTs',plottype='boxplotDF',yscale="symlog")
    story.append(im)

    im= simplePlot(data=statsFreqInterRecepMsgGroupedByGtwDf,x='gatewayname',y='mean',plottype='barErrplotDF',yscale="symlog", yerr='std')
    story.append(im)

    im= groupedPolarPlot(data=dfDevice,groupby="gatewayname",x="az12Deg",y="distance",z="rssi",nrows=2, ncols=2)
    story.append(im)

    # number of gateway
    gtwName = dfDevice['gatewayid'].unique()

    dfDevice['sector']= dfDevice.apply(sectorIdentifier, axis=1, args=('az12Deg',))

    #Discretize the rssi values
    dfDevice['groupRssi'] = dfDevice['rssi'].transform(lambda x: pd.cut(x, bins = 20).astype(str))
    

    gdfNoCrs = gpd.GeoDataFrame(
    dfDevice, geometry=gpd.points_from_xy(dfDevice['objlng'], dfDevice['objlat']))
    
    #Create grid of cells
    grid=createGrid(gdf,'objlng','objlat',9)

    im = groupedGdfWithBackgrdGridGdfPlot(data1=gdfNoCrs,z1="rssi",groupby="gatewayname",data2=grid,groupedTitleText="gateway",nrows=2)
    story.append(im)

    #Identify in which cell the point is located
    pointInPolysGdf = gpd.sjoin(gdfNoCrs, grid,how='left')
    pointInPolysGdf=pointInPolysGdf.dropna() #delete the rows where the cell does not contain any range of rssi values
    pointInPolysGdf=pointInPolysGdf.drop('index_right', axis=1)

    """
    We compute the conditional probability that a packet P belongs to a cell knowing the range 
    of RSSI and a given cell. Let be the number of packets P of the dataset for which the RSSI belongs to 
    a range of rssi (yD ) and for which the position is in a specific cell over the number of packets P of 
    the dataset for which the RSSI belongs to the range of rssi (yD )
    """
    probaMsgInCellDf = (
        pointInPolysGdf.groupby(['gatewayname','groupRssi', 'idcell']).count() / pointInPolysGdf.groupby(['gatewayname','groupRssi']).count())['rssi']\
        .pipe(pd.DataFrame).rename(columns = {'rssi': 'proba'}
        ).reset_index()

    
    #Some parameters to plot probabily in each cell
    newProbaMsgInCellDf = pd.merge(grid, probaMsgInCellDf, on='idcell',  how='right')
    newProbaMsgInCellDf['coords'] = newProbaMsgInCellDf['geometry'].apply(lambda x: x.representative_point().coords[:])
    newProbaMsgInCellDf['coords'] = [coords[0] for coords in newProbaMsgInCellDf['coords']]
    newProbaMsgInCellDf['tuplesPlot'] = list(zip(newProbaMsgInCellDf['gatewayname'], newProbaMsgInCellDf['groupRssi']))

    im = groupedGdfWithBackgrdGridGdfPlot(data1=newProbaMsgInCellDf,z1="proba",groupby=['gatewayname','groupRssi'],data2=grid,groupedTitleText="tuple",nrows=2, annotate=True, conditionAnnot="tuplesPlot",colorCmap='Greens')
    story.append(im)

    #TODO

    # end
    doc.build(story)
    # end